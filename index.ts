///<reference path="Keys.d.ts" />

interface ControlsManager {
    disableAll(...excludes: number[]):void
    enableAll():void
    canSprintJump(sprint: boolean, jump : boolean): void
}
declare const controlsManager: ControlsManager;